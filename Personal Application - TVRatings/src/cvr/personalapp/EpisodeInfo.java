package cvr.personalapp;

/**
 * EpisodeInfo object
 *
 * @author Chris Van Ry
 *
 */

public class EpisodeInfo {

    public String airedSeason;
    public String airedEpisodeNumber;
    public String siteRating;

    public EpisodeInfo(String airedSeason, String airedEpisodeNumber, String siteRating) {
        this.airedSeason = airedSeason;
        this.airedEpisodeNumber = airedEpisodeNumber;
        this.siteRating = siteRating;
    }

    @Override
    public String toString() {
        return "EpisodeInfo{" +
                "airedSeason='" + airedSeason + '\'' +
                ", airedEpisodeNumber='" + airedEpisodeNumber + '\'' +
                ", siteRating='" + siteRating + '\'' +
                '}';
    }
}
