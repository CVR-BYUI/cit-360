package cvr.personalapp;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.*;
import java.io.*;
import java.util.*;


/**
 * The Data class opens an HTTP connection with the TV Database (thetvdb.com), retrieves JSON data, parses
 * show data and stores it as object instances in an ArrayList.
 *
 * @author Chris Van Ry
 *
 */

public class Data {

    // Open HTTP connection and read JSON data into String
    public static String getHttpContent(String string) {

        String content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader((http.getInputStream())));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) !=null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        // Debug - success
        // System.out.println(content);

        return content;

    }

    // Determine number of pages the JSON output is
    public static int getLastPage(String showUrl) throws IOException {

        // Make API call to retrieve first page of JSON data
        String json = getHttpContent(showUrl);

        // Retrieve last page number of API call
        ObjectMapper mapper = new ObjectMapper();

        JsonNode jsonNode = mapper.readTree(json);
        JsonNode key1Node = jsonNode.path("links");
        String lastPage = key1Node.path("last").asText();

        // Debug - success
        // System.out.println(lastPage);

        return Integer.parseInt(lastPage);

    }

    // Parse JSON values into objects, stored in ArrayList
    public static ArrayList<EpisodeInfo> JSONtoArray(String json) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        ArrayList<EpisodeInfo> episodeRating = new ArrayList<>();

        // Read JSON names and assign values to object properties, exception thrown to main()
        JsonNode jsonNode = mapper.readTree(json);

        // Debug - success
        // System.out.println(jsonNode);

        // Get "data" array
        JsonNode dataNode = jsonNode.path("data");

        if (dataNode.isArray()) {

            // Loop through "data" array
            for (JsonNode node : dataNode) {

                //Debug - success
                // System.out.println(node.path("airedSeason").asText());
                // System.out.println(node.path("airedEpisodeNumber").asText());
                // System.out.println(node.path("siteRating").asText());

                // Store values in variables
                String airedSeason = node.path("airedSeason").asText();
                String airedEpisodeNumber = node.path("airedEpisodeNumber").asText();
                String siteRating = node.path("siteRating").asText();

                // Create instance of object with variable properties
                EpisodeInfo epInfo = new EpisodeInfo(airedSeason, airedEpisodeNumber, siteRating);

                // Store object in ArrayList
                episodeRating.add(epInfo);
            }
        }

        // Debug - success
        // System.out.println(episodeRating);

        return episodeRating;
    }

    public static ArrayList getData() {

        // TV show TVDB ID for The Simpsons. This could be replaced with any other show or integrated
        // into a search feature.
        String showId = "71663";
        String apiPageNumber = "1";
        int lastPageNumber = 1;

        // The Simpsons ID = 71663
        // Futurama ID = 73871
        // The X-Files ID = 77398

        // First part of TVDB API call
        String showUrl = "https://api.thetvdb.com/series/" + showId + "/episodes?page=" + apiPageNumber;

        // Get last page number of API call - different shows will have varying results
        try {
            lastPageNumber = getLastPage(showUrl);
        } catch (IOException e) {
            System.err.println(e.toString());
        }

        // Initialize ArrayList of objects
        ArrayList<EpisodeInfo> episodeRating = new ArrayList<>();

        // Iterate through each API call pages of JSON data and store in ArrayList
        for (int i = 1; i <= lastPageNumber; i++) {
            apiPageNumber = String.valueOf(i);
            showUrl = "https://api.thetvdb.com/series/" + showId + "/episodes?page=" + apiPageNumber;
            try {
                String json = getHttpContent(showUrl);
                ArrayList<EpisodeInfo> newData = JSONtoArray(json);
                // Merge new data with existing ArrayList
                episodeRating.addAll(newData);
            } catch (IOException e) {
                System.err.println(e.toString());
            }
        }

        // Debug - success
        // System.out.println(episodeRating);
        return episodeRating;
    }

}