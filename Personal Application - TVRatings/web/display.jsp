<%@ page import="cvr.personalapp.Data" %>
<%@ page import="cvr.personalapp.EpisodeInfo" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: cvanr
  Date: 12/11/2020
  Time: 12:07 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="eng"><head><meta charset="UTF-8">
<title>CIT 360 | TV Ratings</title>
<link rel="stylesheet" href="css/style.css" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $('.cell').each(function () {
            let num = parseFloat($(this).text());
            if (num >= 8) {
                $(this).css('background-color', '#008000');
            } else if (num < 8 && num >= 7) {
                $(this).css('background-color', '#FFF000');
            } else if (num < 7 && num >= 6.5) {
                $(this).css('background-color', '#FFA500');
            } else {
                $(this).css('background-color', '#FF0000');
            }
        });
    });


</script>
</head>
<body>

    <div class="display-box">

        <h1>The Simpsons</h1>

        <table id="rating-table">

            <tr><th>Season</th><th>Episode Number</th><th>User Rating</th></tr>
            <%
                // Retrieve data from Data class
                ArrayList<EpisodeInfo> data = Data.getData();

                // Initialize iterator
                Iterator<EpisodeInfo> iterator = data.iterator();

                while(iterator.hasNext())
                {
                    EpisodeInfo epInfo = iterator.next();
                    %>
                    <tr><td><%=epInfo.airedSeason%></td>
                        <td><%=epInfo.airedEpisodeNumber%></td>
                        <td class="cell"><%=epInfo.siteRating%></td>
                    </tr>
                    <%
                }
            %>
        </table>

        <button type="button" class="submit-button" onclick="javascript:history.back()">Start Over</button>

    </div>

</body>
</html>