<%--
  Created by IntelliJ IDEA.
  User: cvanr
  Date: 12/8/2020
  Time: 10:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="UTF-8">
  <title>CIT 360 | TV Ratings</title>
  <link rel="stylesheet" href="css/style.css" media="screen">
</head>
<body>

  <div class="display-box">

    <h1>TV RATINGS</h1>
    <br>
    <h2>This web application will display TVDB user ratings for each episode of a TV series.</h2>
    <br>
    <h3>Select one of the following TV Series:</h3>
    <br>

    <form action="tvratings" method="post">
      <input type="submit" name="show-selection" class="submit-button" value="The Simpsons">
    </form>

    <h3>Source: <a href="https://thetvdb.com/">thetvdb.com</a></h3>

  </div>

</body>
</html>
