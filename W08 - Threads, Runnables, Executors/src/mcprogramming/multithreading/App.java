package mcprogramming.multithreading;

/**
 * Multithreading - Example
 * @author McProgramming
 * https://www.youtube.com/watch?v=5kTh-dMnBWo
 *
 */
class Birthday implements Runnable
{

    public void run() {
        countDown();
    }

    public void countDown(){
        for(int i = 10; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Happy Birthday!");
    }

}
public class App {

    public static void main(String[] args) {
        Birthday bd = new Birthday();

        Thread t = new Thread(bd);
        t.start();

    }
}
