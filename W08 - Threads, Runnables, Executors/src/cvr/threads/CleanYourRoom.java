package cvr.threads;

/**
 * Threads - Thread 2
 * @author Chris Van Ry
 *
 */

// Implementing runnable makes it a thread
public class CleanYourRoom implements Runnable{

    private int startTime;
    private String name;

    // Constructor
    public CleanYourRoom(String name, int startTime){

        this.name = name;
        this.startTime = startTime;

    }

    public void run(){

        for(int i = 1; i <= 5; i++) {

            // Controls when thread can run
            try {
                Thread.sleep(startTime * 1000);
            }
            catch (InterruptedException e) {
                System.err.println(e.toString());
            }

            // Display thread output
            System.out.println(name + ", it's time to clean your room.");
            System.out.println("Clean room count: " + i + "\n");

        }
    }

}
