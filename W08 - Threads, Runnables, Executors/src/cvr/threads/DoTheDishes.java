package cvr.threads;

/**
 * Threads - Thread 3
 * @author Chris Van Ry
 *
 */

// Implementing runnable makes it a thread
public class DoTheDishes implements Runnable{

    private int startTime;
    private String name;

    // Constructor
    public DoTheDishes(String name, int startTime){

        this.name = name;
        this.startTime = startTime;

    }

    public void run(){

        for(int i = 1; i <= 10; i++) {

            // Controls when thread can run
            try {
                Thread.sleep(startTime * 1000);
            }
            catch (InterruptedException e) {
                System.err.println(e.toString());
            }

            // Display thread output
            System.out.println(name + ", it's time to do the dishes.");
            System.out.println("Dishes count: " + i + "\n");

        }
    }

}
