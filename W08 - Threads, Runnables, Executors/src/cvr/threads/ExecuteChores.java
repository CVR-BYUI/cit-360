package cvr.threads;

/**
 * Threads - Execute Chores (Main executor)
 * @author Chris Van Ry
 *
 */

// Executors provide thread management
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteChores {

    public static void main(String[] args){

        // Specify how many threads can execute concurrently
        ExecutorService ChoreOne = Executors.newFixedThreadPool(3);

        // Runnables
        WalkTheDog co1 = new WalkTheDog("Ian", 1);
        WalkTheDog co2 = new WalkTheDog("Evelyn", 2);
        WalkTheDog co3 = new WalkTheDog("Calvin", 3);
        WalkTheDog co4 = new WalkTheDog("Benjamin", 4);

        // Specify how many threads can execute concurrently
        ExecutorService ChoreTwo = Executors.newFixedThreadPool(3);

        // Runnables
        CleanYourRoom ct1 = new CleanYourRoom("Ian", 1);
        CleanYourRoom ct2 = new CleanYourRoom("Evelyn", 2);
        CleanYourRoom ct3 = new CleanYourRoom("Calvin", 3);
        CleanYourRoom ct4 = new CleanYourRoom("Benjamin", 4);

        // Specify how many threads can execute concurrently
        ExecutorService ChoreThree = Executors.newFixedThreadPool(3);

        // Runnables
        DoTheDishes cth1 = new DoTheDishes("Ian", 1);
        DoTheDishes cth2 = new DoTheDishes("Evelyn", 2);
        DoTheDishes cth3 = new DoTheDishes("Calvin", 3);
        DoTheDishes cth4 = new DoTheDishes("Benjamin", 4);

        // Specify how many threads can execute concurrently
        ExecutorService ChoreFour = Executors.newFixedThreadPool(3);

        // Runnables
        TakeOutGarbage cf1 = new TakeOutGarbage("Ian", 1);
        TakeOutGarbage cf2 = new TakeOutGarbage("Evelyn", 2);
        TakeOutGarbage cf3 = new TakeOutGarbage("Calvin", 3);
        TakeOutGarbage cf4 = new TakeOutGarbage("Benjamin", 4);

        // Execute runnables
        ChoreOne.execute(co1);
        ChoreOne.execute(co2);
        ChoreOne.execute(co3);
        ChoreOne.execute(co4);

        ChoreTwo.execute(ct1);
        ChoreTwo.execute(ct2);
        ChoreTwo.execute(ct3);
        ChoreTwo.execute(ct4);

        ChoreThree.execute(cth1);
        ChoreThree.execute(cth2);
        ChoreThree.execute(cth3);
        ChoreThree.execute(cth4);

        ChoreFour.execute(cf1);
        ChoreFour.execute(cf2);
        ChoreFour.execute(cf3);
        ChoreFour.execute(cf4);

        // Shutdown threadpool executor service - stop program once threads are done
        ChoreOne.shutdown();
        ChoreTwo.shutdown();
        ChoreThree.shutdown();
        ChoreFour.shutdown();
    }

}
