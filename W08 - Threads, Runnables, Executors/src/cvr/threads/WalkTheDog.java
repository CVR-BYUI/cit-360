package cvr.threads;

/**
 * Threads - Thread 1
 * @author Chris Van Ry
 *
 */

// Implementing runnable makes it a thread
public class WalkTheDog extends Thread {

    private int startTime;
    private String name;

    // Constructor
    public WalkTheDog(String name, int startTime) {

        this.name = name;
        this.startTime = startTime;

    }

    public void run(){

        for(int i = 1; i <= 15; i++){

            // Controls when thread can run
            try{
                Thread.sleep(startTime * 1000);
            }
            catch (InterruptedException e) {
                System.err.println(e.toString());
            }

            // Display thread output
            System.out.println(name + ", it's time to walk the dog.");
            System.out.println("Dog walk count: " + i + "\n");

        }

    }
}
