package cvr.threads;

/**
 * Threads - Thread 4
 * @author Chris Van Ry
 *
 */

// Implementing runnable makes it a thread
public class TakeOutGarbage implements Runnable{

    private int startTime;
    private String name;

    // Constructor
    public TakeOutGarbage(String name, int startTime){

        this.name = name;
        this.startTime = startTime;

    }

    public void run(){

        for(int i = 1; i <= 5; i++) {

            // Controls when thread can run
            try {
                Thread.sleep(startTime * 1000);
            }
            catch (InterruptedException e) {
                System.err.println(e.toString());
            }

            // Display thread output
            System.out.println(name + ", it's time to take out the garbage.");
            System.out.println("Garbage count: " + i + "\n");

        }
    }

}