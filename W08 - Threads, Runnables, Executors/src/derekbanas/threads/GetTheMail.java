package derekbanas.threads;

/**
 * Threads - Thread 2
 * @author Derek Banas
 * https://youtu.be/F-CkaU8aQZI
 *
 */

public class GetTheMail implements Runnable{

    private int startTime;

    public GetTheMail(int startTime){
        this.startTime = startTime;
    }

    public void run(){

        try{
            Thread.sleep(startTime * 1000);
        }
        catch(InterruptedException e)
        {}
        System.out.println("Checking Mail");
    }
}
