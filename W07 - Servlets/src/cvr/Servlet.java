package cvr;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        out.println("<!DOCTYPE html><html lang=\"eng\"><head><meta charset=\"UTF-8\">");
        out.println("<title>CIT 360 | W07 Servlets</title>");
        out.println("<link rel=\"stylesheet\" href=\"css/style.css\" media=\"screen\">");
        out.println("</head><body>");

        String courseOne = request.getParameter("courseOne");
        String courseTwo = request.getParameter("courseTwo");
        String courseThree = request.getParameter("courseThree");
        String courseFour = request.getParameter("courseFour");

        out.println("<h1>Course Registration</h1><br>");
        out.println("<h3>Here is your schedule:</h3><br>");
        out.println("<table><tr><th>Course</th><th>Schedule</th><th>Instructor</th></tr>");
        out.println("<tr><th>" + courseOne + "</th><th>M W F</th><th>Hinckley</th></tr>");
        out.println("<tr><th>" + courseTwo + "</th><th>T Th</th><th>Tuckett</th></tr>");
        out.println("<tr><th>" + courseThree + "</th><th>M T W Th F</th><th>Smith</th></tr>");
        out.println("<tr><th>" + courseFour + "</th><th>M W F</th><th>Canlas</th></tr></table>");
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("This resource is not available directly.");

    }
}
