package cvr.jsonandhttp;

/**
 * JSON and HTTP/URL
 * @author Chris Van Ry
 *
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.*;
import java.io.*;

public class JSONandHTTP {

    // Open HTTP connection and read JSON data into a string
    public static String getHttpContent(String string) {

        String content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader((http.getInputStream())));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) !=null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;

    }

    // Convert object into JSON format
    public static String weathertoJSON(Weather weather) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";

        // Convert object properties into JSON string
        jsonString = mapper.writeValueAsString(weather);

        return jsonString;
    }

    // Convert JSON data into object
    public static Weather JSONToWeather(String s) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Weather currentWeather = null;

        // Read JSON names and assign values to object properties, exception thrown to main()
        currentWeather = mapper.readValue(s, Weather.class);

        return currentWeather;
    }

    public static void main(String[] args) {

        // Part 1 - convert object to JSON
        Weather currentWeather1 = new Weather();
        currentWeather1.setCity("Salt Lake City");
        currentWeather1.setCondition("Overcast");
        currentWeather1.setCurrentTemp("60");
        currentWeather1.setHiTemp("65");
        currentWeather1.setLoTemp("55");

        System.out.println("Part 1: An object is created, converted to JSON format, and displayed.\n");

        try {
            String json = weathertoJSON(currentWeather1);
            System.out.println(json);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        // Part 2 - convert JSON to object
        String jsonURL = getHttpContent("https://bitbucket.org/CVR-BYUI/cit-360/raw/1514949f4bca9bf03cb5f0af5527d9628b02405d/W04%20-%20JSON%20and%20HTTP-URL/src/cvr/jsonandhttp/weather.json");

        System.out.println("\nPart 2: HTTP connection will retrieve JSON data, convert into an object, and display it\n");

        try {
            Weather currentWeather2 = JSONToWeather(jsonURL);
            System.out.println(currentWeather2.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
