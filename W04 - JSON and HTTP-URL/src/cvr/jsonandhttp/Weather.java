package cvr.jsonandhttp;

public class Weather{

    private String city;
    private String condition;
    private String currentTemp;
    private String hiTemp;
    private String loTemp;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(String currentTemp) {
        this.currentTemp = currentTemp;
    }

    public String getHiTemp() {
        return hiTemp;
    }

    public void setHiTemp(String hiTemp) {
        this.hiTemp = hiTemp;
    }

    public String getLoTemp() {
        return loTemp;
    }

    public void setLoTemp(String loTemp) {
        this.loTemp = loTemp;
    }

    @Override
    public String toString() {
        return "City: " + city + "\n[ Current Weather ] " + "\nCondition: " + condition + "\nTemperature: "
                + currentTemp + "\nHigh: " + hiTemp + "\nLow: " + loTemp;
    }

}
