package cvr.junit2;

import junit.runner.Version;

/**
 * JUnit class - simple methods to test using JUnit
 * @author Chris Van Ry
 *
 */

// JUnit testing is used to verify small sections of code at a time. In this case, we are
// testing simple methods.

public class JUnit {

    // Addition
    public static int sum(int num1, int num2) {
        return num1 + num2;
    }

    // Subtraction
    public static int difference(int num1, int num2) {
        return num1 - num2;
    }

    // Multiplication
    public static int multiply(int num1, int num2) {
        return num1 * num2;
    }

    // Division
    public static int divide(int num1, int num2) {

        int quotient = 0;

        try {
            quotient = num1 / num2;
        }
        catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }

        return quotient;
    }

    // Array - even numbers
    public static int[] buildEvenArray() {
        int[] newArray = {2, 4, 6, 8};
        return newArray;
    }

    // Array - odd numbers
    public static int[] buildOddArray() {
        int[] newArray = {1, 3, 5, 7};
        return newArray;
    }

    // Boolean
    public static boolean determineTrue(String value) {
        boolean result = Boolean.parseBoolean(value);
        return result;
    }

    // Null/not null string
    public static String makeStatement(String value) {
        String result;
        if (value.equals("This is null")) {
            result = null;
        }
        else {
            result = "not null";
        }
        return result;
    }

    // JUnit version
    public static void main (String args[]) {
        System.out.println("JUnit version is: " + Version.id());
    }

}
