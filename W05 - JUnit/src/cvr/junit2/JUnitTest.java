package cvr.junit2;

/**
 * JUnitTest class - tests whether methods pass or fail using the 8 of the 9 JUnit assertion methods
 * @author Chris Van Ry
 *
 */

// THe following makes use of 8 of the 9 JUnit assertion method types to test different results.

import org.junit.Test;
import static org.junit.Assert.*;

public class JUnitTest {

    // assertEquals - test pass
    // Sum equals 11
    @Test
    public void testSum1() {
        int value = JUnit.sum(8, 3);
        assertEquals(value, 11);
    }

    // assertEquals - test fail
    // Sum doesn't equal 25
    @Test
    public void testSum2() {
        int value = JUnit.sum(8, 3);
        assertEquals(value, 25);
    }

    // assertEquals - test pass
    // Difference equals 10
    @Test
    public void testSubtract1() {
        int value = JUnit.difference(20, 10);
        assertEquals(value, 10);
    }

    // assertEquals - test fail
    // Difference doesn't equal 5
    @Test
    public void testSubtract2() {
        int value = JUnit.difference(20, 10);
        assertEquals(value, 5);
    }

    // assertEquals - test pass
    // Product equals 25
    @Test
    public void testMultiply1() {
        int value = JUnit.multiply(5, 5);
        assertEquals(value, 25);
    }

    // assertEquals - test fail
    // Product doesn't equal 50
    @Test
    public void testMultiply2() {
        int value = JUnit.multiply(5, 5);
        assertEquals(value, 50);
    }

    // assertEquals - test pass
    // Quotient equals 10
    @Test
    public void testDivide1() {
        int value = JUnit.divide(60, 6);
        assertEquals(value, 10);
    }

    // assertEquals - test fail
    // Quotient doesn't equal 40
    @Test
    public void testDivide2() {
        int value = JUnit.divide(60, 6);
        assertEquals(value, 40);
    }

    // assertArrayEquals - test pass
    // Array is the same
    @Test
    public void testArray1() {
        int[] array = {2, 4, 6, 8};
        assertArrayEquals(array, JUnit.buildEvenArray());
    }

    // assertArrayEquals - test fail
    // Array isn't the same
    @Test
    public void testArray2() {
        int[] array = {2, 4, 6, 8};
        assertArrayEquals(array, JUnit.buildOddArray());
    }

    // assertTrue - test pass
    // Statement is true (case doesn't matter)
    @Test
    public void testBoolean1() {
        String value = "TrUe";
        assertTrue(value, JUnit.determineTrue(value));
    }

    // assertTrue - test fail
    // Statement is not true
    @Test
    public void testBoolean2() {
        String value = "This statement is totally true. Trust me.";
        assertTrue(value, JUnit.determineTrue(value));
    }

    // assertFalse - test pass
    // Statement is false
    @Test
    public void testBoolean3() {
        String value = "This statement is totally true. Trust me.";
        assertFalse(value, JUnit.determineTrue(value));
    }

    // assertFalse - test pass
    // Statement is not false (case doesn't matter)
    @Test
    public void testBoolean4() {
        String value = "tRuE";
        assertFalse(value, JUnit.determineTrue(value));
    }

    // assertNull - test pass
    // String match - equals null
    @Test
    public void testNull1() {
        String value = "This is null";
        assertNull(JUnit.makeStatement(value));
    }

    // assertNull - test fail
    // String doesn't match - not null
    @Test
    public void testNull2() {
        String value = "This is not null";
        assertNull(JUnit.makeStatement(value));
    }

    // assertNotNull - test pass
    // String doesn't match - not null
    @Test
    public void testNull3() {
        String value = "This is not null";
        assertNotNull(JUnit.makeStatement(value));
    }

    // assertNotNull - test fail
    // String match - equals null
    @Test
    public void testNull4() {
        String value = "This is null";
        assertNotNull(JUnit.makeStatement(value));
    }

    // assertSame - test pass
    // Values are the same
    @Test
    public void testSame1() {
        int value = 2;
        assertSame(JUnit.sum(value, value), JUnit.multiply(value, value));
    }

    // assertSame - test fail
    // Values aren't the same
    @Test
    public void testSame2() {
        int value = 3;
        assertSame(JUnit.sum(value, value), JUnit.multiply(value, value));
    }

    // assertNotSame - test pass
    // Values aren't the same
    @Test
    public void testNotSame1() {
        int value = 3;
        assertNotSame(JUnit.sum(value, value), JUnit.multiply(value, value));
    }

    // assertNotSame - test fail
    // Values are the same
    @Test
    public void testNotSame2() {
        int value = 2;
        assertNotSame(JUnit.sum(value, value), JUnit.multiply(value, value));
    }

}
