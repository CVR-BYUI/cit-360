package cvr.junit;

import java.util.Scanner;

public class ComputeGrade {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // Describe program
        System.out.println("This program will convert a number grade into a letter grade.\n");

        // Prompt user for input
        System.out.print("Enter number grade: ");
        int grade = input.nextInt();

        // Compute letter grade and sign
        char letterGrade = computeLetterGrade(grade);
        char gradeSign = computeGradeSign(grade);

        // Display results
        System.out.println("\nYour letter grade is " + letterGrade + gradeSign);

    }

    public static char computeLetterGrade(int grade) {

        switch (grade / 10) {
            case 10:
            case 9:
                return 'A';
            case 8:
                return 'B';
            case 7:
                return 'C';
            case 6:
                return 'D';
            default:
                return 'F';

        }
    }

    public static char computeGradeSign(int grade) {

        if (grade == 100) {
            return '+';
        }
        if (grade < 60) {
            return 0;
        }

        switch (grade % 10) {
            case 0:
            case 1:
            case 2:
                return '-';
            case 7:
            case 8:
            case 9:
                return '+';
            default:
                return 0;

        }
    }

}
