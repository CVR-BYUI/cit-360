package cvr.junit;

import static org.junit.Assert.*;

class ComputeGradeTest {

    @org.junit.jupiter.api.Test
    void computeLetterGrade() {
        ComputeGrade test = new ComputeGrade();
        char output = test.computeLetterGrade(100);
        assertEquals('A', output);
    }

    @org.junit.jupiter.api.Test
    void computeGradeSign() {
        ComputeGrade test = new ComputeGrade();
        char output = test.computeGradeSign(88);
        assertEquals('+', output);

    }
}