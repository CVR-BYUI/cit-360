package cvr.collections;

public class FamilyMember implements Comparable<FamilyMember> {

    String firstName;
    String middleName;
    int memberAge;

    FamilyMember(String first, String middle, int age){
        this.firstName = first;
        this.middleName = middle;
        this.memberAge = age;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public int getMemberAge() {
        return memberAge;
    }
    public void setMemberAge(int memberAge) {
        this.memberAge = memberAge;
    }
    @Override
    public int compareTo(FamilyMember fam){
        return this.firstName.compareTo(fam.firstName);
    }
}
