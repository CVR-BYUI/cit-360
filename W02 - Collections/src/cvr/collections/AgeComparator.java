package cvr.collections;

import java.util.Comparator;

public class AgeComparator implements Comparator<FamilyMember> {

    public int compare(FamilyMember f1,FamilyMember f2){
        if(f1.memberAge==f2.memberAge)
            return 0;
        else if(f1.memberAge>f2.memberAge)
            return 1;
        else
            return -1;
    }
}
