package cvr.collections;

/**
 * Collection Types
 * @author Chris Van Ry
 *
 */

import java.util.*;

public class CollectionsExample {

    // Java Collections Framework: contains collection interfaces and classes. Like arrays, it contains values
    // (references to other objects). Unlike arrays, collections can dynamically grow and shrink in size.

    public static void main(String[] args) {

        // Collections: List
        // The list method will build sequentially in the order elements are added to it.

        System.out.println("----- List -----\n");

        System.out.println("A list can be created by declaring the variable and adding to it.\n");
        List list1 = new ArrayList();
        list1.add("New York City");
        list1.add("Albany");
        list1.add("Niagara Falls");
        list1.add("Buffalo");
        list1.add("Rochester");
        list1.add("Syracuse");

        // Display list1
        for (Object str : list1) {
            System.out.println((String) str);
        }

        System.out.println("\n---\n");

        System.out.println("An array can also be added to a list.\n");
        String[] array = {"New York City", "Albany", "Niagara Falls", "Buffalo", "Rochester", "Syracuse"};
        List<String> list2 = new ArrayList<String>();
        for (String x : array)
            list2.add(x);

        // Display list2
        for (Object str : list2) {
            System.out.println((String) str);
        }

        System.out.println("\n---\n");

        Scanner input = new Scanner(System.in);
        List list3 = new ArrayList();
        Integer counter = 1;

        System.out.println("A list can be created by user input.\nFor each prompt, provide the " +
                "name of a city in Utah.\n");

        // Validate user input
        while (counter < 6) {
            System.out.print("City " + counter + ": ");
            String value = input.nextLine();
            if (value.length() > 0 && value.length() < 30) {
                list3.add(value);
                counter++;
            }
            else {
                System.out.println("Invalid input. Try again.");
            }
        }

        System.out.print("\n");

        // Display list3
        for (Object str : list3) {
            System.out.println((String) str);
        }

        // Collections: Set
        // A set differs from a list in that it doesn't allow duplicate objects and elements are not stored
        // sequentially.

        System.out.println("\n----- Set -----\n");

        // TreeSet - elements are sorted in ascending order, by default.
        System.out.println("TreeSet will display alphabetically. \"Buffalo\" was added twice to this set, " +
                "but only one will display.\n");
        Set set1 = new TreeSet();
        set1.add("New York City");
        set1.add("Albany");
        set1.add("Utica");
        set1.add("Buffalo");
        set1.add("Rochester");
        set1.add("Syracuse");
        set1.add("Buffalo");

        // Display set1
        for (Object str : set1) {
            System.out.println((String) str);
        }

        System.out.println("\n---\n");

        //HashSet - elements are not in any sort of order.
        System.out.println("HashSet will display randomly. \"New York City\" was added twice to this set, " +
                "but only one will display.\n");
        Set set2 = new HashSet();
        set2.add("New York City");
        set2.add("Albany");
        set2.add("Utica");
        set2.add("Buffalo");
        set2.add("Rochester");
        set2.add("Syracuse");
        set2.add("New York City");

        // Display set2
        for (Object str : set2) {
            System.out.println((String) str);
        }

        // Collections: Queue
        // Queue follows the FIFO principle (First-In-First-Out. Elements are placed at the end of the queue
        // and removed from the beginning.

        System.out.println("\n----- Queue -----\n");

        // PriorityQueue - Capital letters first, then alphabetized.
        System.out.println("PriorityQueue will display capitalized words first, then lower case in alphabetical " +
                "order \n");
        Queue queue1 = new PriorityQueue();
        queue1.add("I");
        queue1.add("will");
        queue1.add("go");
        queue1.add("and");
        queue1.add("do");
        queue1.add("the");
        queue1.add("things");
        queue1.add("which");
        queue1.add("the");
        queue1.add("Lord");
        queue1.add("hath");
        queue1.add("commanded");

        // Display queue1 by iterating through it
        Iterator iterator1 = queue1.iterator();
        while (iterator1.hasNext()) {
            System.out.println(queue1.poll());
        }

        // Collections: Deque
        // Deque is a queue that allows adding and removing elements from both sides.

        System.out.println("\n----- Dequeue -----\n");

        // LinkedList - allows dynamic memory allocation.
        System.out.println("LinkedList elements are linked to each other using the reference part of the node " +
                "that contains the address of the next node on the list.\n");
        LinkedList<String> list4 = new LinkedList<String>();
        list4.add("2 Nephi");
        list4.add("Jacob");
        list4.add("Enos");
        list4.add("Jarom");
        list4.add("Omni");
        list4.add("Words of Mormon");
        list4.add("Alma");
        list4.add("Helaman");
        list4.add("3 Nephi");
        list4.add("4 Nephi");
        list4.add("Mormon");
        list4.add("Ether");

        //Adding an element to the first position
        list4.addFirst("1 Nephi");

        //Adding an element to the last position
        list4.addLast("Moroni");

        //Adding an element to the 8th position
        list4.add(7, "Mosiah");

        // Display list4 by iterating through it
        Iterator iterator2 = list4.iterator();
        while (iterator2.hasNext()) {
            System.out.println(list4.poll());
        }

        // Collections: Map
        // Map interface utilizes a mapping between key and value.

        System.out.println("\n----- Map -----\n");

        // HashMap - basic implementation of map interface. Elements of the Hashmap are unordered.
        System.out.println("HashMap maps the key and value together but doesn't keep elements in any " +
                "particular order.\n");
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put("Austin", "Texas");
        map1.put("Boise", "Idaho");
        map1.put("Atlanta", "Georgia");
        map1.put("Boston", "Massachusetts");
        map1.put("Sacramento", "California");
        map1.put("Charleston", "West Virginia");
        map1.put("Trenton", "New Jersey");

        // Display map1 keys
        System.out.println("HashMap keys:\n");
        for (String i : map1.keySet()) {
            System.out.println(i);
        }

        // Display map1 values
        System.out.println("\nHashMap values:\n");
        for (String i : map1.values()) {
            System.out.println(i);
        }

        // Display both keys and values
        System.out.println("\nHashMap keys and values:\n");
        for (String i : map1.keySet()) {
            System.out.println(i + ", " + map1.get(i));
        }

        System.out.println("\n---\n");

        // TreeMap - basic implementation of map interface.
        System.out.println("Treemap maps the key and value together and sorts in ascending order of its keys. " +
                "Multiple values with the same key will display the last iteration (true for both HashMap and " +
                "TreeMap).\n");
        Map<Integer, String> map2 = new HashMap<Integer, String>();
        map2.put(5, "Diet Coke");
        map2.put(5, "Root Beer");
        map2.put(2, "like");
        map2.put(1, "I");
        map2.put(3, "to");
        map2.put(4, "drink");
        map2.put(5, "water");

        // Display map2
        for (int i = 1; i < 6; i++) {
            String result = (String)map2.get(i);
            System.out.println(result);
        }

        System.out.println("\n---\n");

        // Comparator interface implementation
        System.out.println("The Comparator interface allows sorting objects based on any data member.\n");

        // List of objects of FamilyMember class
        ArrayList<FamilyMember> list5 = new ArrayList<FamilyMember>();
        list5.add(new FamilyMember("Christopher", "John",  36));
        list5.add(new FamilyMember("Damita", "Leann", 33));
        list5.add(new FamilyMember("Ian", "Kenneth", 6));
        list5.add(new FamilyMember("Evelyn", "Christine", 3));
        list5.add(new FamilyMember("Calvin", "Richard", 1));
        list5.add(new FamilyMember("Benjamin", "Christopher", 1));

        System.out.println("Sorting by First Name:");
        Collections.sort(list5);
        for(FamilyMember fam: list5){
            System.out.println(fam.getFirstName() + ", " + fam.getMiddleName() + ", " +
                    fam.getMemberAge());
        }

        System.out.print("\n");

        System.out.println("Sorting by Middle Name:");
        Collections.sort(list5, new MiddleNameComparator());
        for(FamilyMember fam: list5) {
            System.out.println(fam.getFirstName() + ", " + fam.getMiddleName() + ", " +
                    fam.getMemberAge());
        }

        System.out.print("\n");

        System.out.println("Sorting by Age:");
        Collections.sort(list5, new AgeComparator());
        for(FamilyMember fam: list5) {
            System.out.println(fam.getFirstName() + ", " + fam.getMiddleName() + ", " +
                    fam.getMemberAge());
        }
    }
}