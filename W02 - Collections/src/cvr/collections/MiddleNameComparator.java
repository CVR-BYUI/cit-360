package cvr.collections;

import java.util.Comparator;

public class MiddleNameComparator implements Comparator<FamilyMember> {

    public int compare(FamilyMember f1,FamilyMember f2){
        return f1.middleName.compareTo(f2.middleName);
    }
}
