package cvr.exceptions;

/**
 * Exception Handling and Data Validation
 * @author Chris Van Ry
 *
 */

import java.util.*;

public class ExceptionsExample {

    public static void main(String[] args) {

        int num1;
        int num2;
        float result;
        String answer;
        boolean tryAgain = true;

        Scanner numInput = new Scanner(System.in);
        Scanner answerInput = new Scanner(System.in);

        do {
            // Prompt for user input
            System.out.print("Please enter the first number: ");
            num1 = numInput.nextInt();

            // Call isZero method
            do {
                System.out.print("Please enter the second number: ");
                num2 = numInput.nextInt();
            }
            while (isZero(num2));

            // Call doDivision method
            try {
                result = doDivision(num1, num2);
                System.out.println(num1 + " / " + num2 + " = " + result);

            // Exception thrown from doDivision method
            } catch (Exception e) {
                System.out.println("Error: Can't divide by zero. (" + e + ").");
            }

            System.out.print("Try Again? (Y/N): ");
            answer = answerInput.nextLine();

            if (answer.equalsIgnoreCase("n")) {
                tryAgain = false;
            }

        }
        while (tryAgain);

        System.out.println("Goodbye!");

    }

    // This method throws the exception to the caller, since it is not used directly within.
    public static float doDivision(int num1, int num2) throws ArithmeticException {
        float result = num1 / num2;
        return result;
    }

    // This method validates the second number and returns a boolean value
    public static boolean isZero(int num) {
        if (num == 0) {
            System.out.println("Invalid input. Try Again.");
            return true;
        }
        else {
            return false;
        }
    }
}
